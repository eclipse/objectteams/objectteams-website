<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/objectteams/css/style.css"/>');

	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Getting involved with Object Teams";
	$pageKeywords	= "ObjectTeams, contribute, forum, source code, repository";
	$pageAuthor		= "Stephan Herrmann";
		
	# Paste your HTML content between the EOHTML markers!	
	ob_start();
	?>
	<div id="midcolumn">
		<h1>Getting involved with Object Teams</h1>
		
		<h2>Forum</h2>
		<p>
			<strong class="orange"><font size="+1">ASK</font></strong>: Should you have any questions regarding Object Teams the primary place to ask is the
			<a href="http://www.eclipse.org/forums/eclipse.objectteams">Object Teams forum</a>.
		</p>

		<h2>Wiki</h2>
		<p>
			<strong class="orange"><font size="+1">READ</font></strong>: Categories of the Object Teams Wiki:
			<ul style="margin-top:0px;">
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams">Object Teams</a></li>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams_Examples">Object Teams Examples</a></li>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams_Patterns">Object Teams Patterns</a></li>
			<li><a href="http://wiki.eclipse.org/Category:OTEquinox">OT/Equinox</a></li>
			</ul>
		</p>
		
		<h2>Source code</h2>
		<p>
			<strong class="orange"><font size="+1">CHECK-OUT</font></strong>: Our source code is managed in this <a href="http://git.eclipse.org/c/objectteams/org.eclipse.objectteams.git">GIT Repository</a>.<br/>
			Repository content:
			<dl style="margin-left:30px;margin-top:0px;margin-bottom:10px;">
			<dt>/</dt>
			<dd>forked versions of jdt plug-ins: <code>org.eclipse.jdt.core</code> and corresponding tests</dd>
			<dt>plugins</dt>
			<dd>all plug-ins for OT/J, OT/Equinox and the OTDT</dd>
			<dt>features</dt>
			<dd>the features used to build and provision OT/Equinox and the OTDT</dd>
			<dt>testplugins</dt>
			<dd>JUnit Plug-in Tests for the OTDT</dd>
			<dt>releng</dt>
			<dd>Scripts and map files for building and testing the OTDT, see <a href="http://wiki.eclipse.org/OTBuilding">OTBuilding</a> in the wiki for more information.</dd>
			<dt>othersrc</dt>
			<dd>sources that are used indirectly: otdt-examples (as zip files inside plug-in <code>org.eclipse.objectteams.otdt.samples</code>) and JCLMin1.5 (class library stubs used by tests).</dd>
			<dt>maven</dt>
			<dd>POM files for distributing and using OT/J in maven builds.</dd>
			<dt>contrib</dt>
			<dd>Projects not included in the regular OTDT distribution (currently a prototype for adding annotation based null-analysis to the JDT).</dd>
			</dl>
			For development in the IDE it is recommended to install Eclipse from these components:
			<ul>
			<li>Eclipse SDK</li>
			<li><a href="download.php">OTDT</a></li>
			<li><a href="http://www.eclipse.org/egit">EGit</a></li>
			</ul>
		</p>
		<p>
			<strong class="orange"><font size="+1">BUILD</font></strong>: For basic steps to compile/build/deploy see
			<a href="http://wiki.eclipse.org/OTDevelopmentSetup">OTDevelopmentSetup</a>.
		</p>
		<p>
			<strong class="orange"><font size="+1">CONTRIBUTE</font></strong>: We welcome everybody interested in contributing.
			<ul style="margin-top:0px;">
			<li>Please see the section
			<a href="http://www.eclipse.org/projects/dev_process/development_process.php#4_7_Committers_and_Contributors">Committers and Contributors</a>
			from the Eclipse Development Process and contact us via the <a href="http://www.eclipse.org/forums/eclipse.objectteams">forum</a>.</li>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams_Development">Developer Documentation</a> in the wiki.</li>
			</ul>
			Please find the list of past and present <a href="http://wiki.eclipse.org/OTFacts#Contributors">Contributors</a> in the wiki.
		</p>
		
	</div> <!-- midcolumn -->

	<!-- remove the entire <div> tag to omit the right column!  -->
	<!--div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="midcolumn.php">#midcolumn Template</a></li>
				<li><a href="fullcolumn.php">#fullcolumn Template</a></li>
			</ul>
		</div>
		<div class="sideitem">
			<h6>&lt;h6&gt; tag</h6>
				<div class="modal">
					Wrapping your content using a div.modal tag here will add the gradient background
				</div>
		</div>
	</div-->

	
	<?
	$html = ob_get_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
