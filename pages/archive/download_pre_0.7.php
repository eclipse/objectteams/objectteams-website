<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	$pageTitle 		= "Object Teams - Archived Downloads";
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/objectteams/css/style.css"/>');

	$html  = <<<EOHTML
<div id="midcolumn">
	<h1>$pageTitle</h1>
	<p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a> unless otherwise specified.</p>
	
		<ul class="novaArrow">
        <li><a href="/objectteams/features.php">Features of the Object Teams Development Tooling (OTDT)</a></li>
        </ul>
	
	<h2>Archived Milestone Release of the OTDT<br>prior to the <a href="/objectteams/download.php">0.7.0 Release</a></h2>
                <p>The Object Teams project has released its first Milestone Releases after the move to Eclipse.org.
                   Due to the incubation status of the project, this release has the version 0.7.0 (M<i>x</i>),
                   which is the direct successor of the stable release 1.4.0 from objectteams.org.</p>

        <table border="0">
            <tr>
                <td><a href="http://archive.eclipse.org/objectteams/updates/0.7"><img align="left" src="http://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" /></a></td>
                <td style="width:100%;padding-left:6px;"><b>Archived update site:</b> <a href="http://archive.eclipse.org/objectteams/updates/0.7">http://archive.eclipse.org/objectteams/updates/0.7</a>
                </td>
            </tr>
        </table>
        <div style="margin-left:30px;" class="sideitem"><!-- use sideitem to achieve gray background -->
            <h6>Available Milestone Releases towards 0.7:</h6>
            <table width="100%">
	            <tr valign="top"><th>&nbsp;</th><th bgcolor="#e8fcff">Required Eclipse Version</th><th bgcolor="#e8fcff">Command Line Compiler</th><th bgcolor="#e8fcff">Bugs Fixed</th></tr>
	            <tr><td colspan=4><strong>OTDT 0.7.0 Release Candidate 1</strong> &mdash; (2010/06/23)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td bgcolor="#e8fcff"><a href="http://download.eclipse.org/eclipse/downloads/drops/R-3.6-201006080911"><img src="../../images/arrow.gif">Eclipse SDK 3.6.0</a></td>
                    <td bgcolor="#e8fcff"><a href="http://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-I-0.7RC1-201006211729.jar"><img src="../../images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td bgcolor="#e8fcff"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7;resolution=FIXED;product=Objectteams"><img title="Bugs fixed for 0.7" src="../../images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan=4><strong>OTDT 0.7.0 Milestone 4</strong> &mdash; (2010/06/11)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td bgcolor="#e8fcff"><a href="http://archive.eclipse.org/eclipse/downloads/drops/S-3.6RC4-201006031500/index.php"><img src="../../images/arrow.gif">Eclipse SDK 3.6.0 RC4</a></td>
                    <td bgcolor="#e8fcff"><a href="http://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-I-0.7M4-201006111044.jar"><img src="../../images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td bgcolor="#e8fcff"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7%20M4;resolution=FIXED;product=Objectteams"><img title="Bugs fixed for 0.7M4" src="../../images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan=4><strong>OTDT 0.7.0 Milestone 3</strong> &mdash; (2010/05/31)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td bgcolor="#e8fcff"><a href="http://archive.eclipse.org/eclipse/downloads/drops/S-3.6RC3-201005271700/index.php"><img src="../../images/arrow.gif">Eclipse SDK 3.6.0 RC3</a></td>
                    <td bgcolor="#e8fcff"><a href="http://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-I201005310634.jar"><img src="../../images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td bgcolor="#e8fcff"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7%20M3;resolution=FIXED;product=Objectteams"><img title="Bugs fixed for 0.7M3" src="../../images/bug.gif"></a></td>
                </tr>
                <tr><td colspan=4><strong>OTDT 0.7.0 Milestone 2</strong> &mdash; (2010/05/07)</td></tr>
                <tr><td>&nbsp;</td>
                    <td bgcolor="#e8fcff"><a href="http://archive.eclipse.org/eclipse/downloads/drops/S-3.6M7-201004291549/index.php"><img src="../../images/arrow.gif"> Eclipse SDK 3.6.0 M7</a></td>
                    <td bgcolor="#e8fcff"><a href="http://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-I201005190940.jar"><img src="../../images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td bgcolor="#e8fcff"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7%20M2;product=Objectteams"><img title="Bugs fixed for 0.7M2" src="../../images/bug.gif"></a></a></td>
                </tr>
                <tr><td colspan=4><strong>OTDT 0.7.0 Milestone 1</strong> &mdash; (2010/04/25)</td></tr>
                <tr><td>&nbsp;</td>
                    <td bgcolor="#e8fcff" colspan="2"><i>This Warm-Up Release is no longer available.</i></td>
                    <td bgcolor="#e8fcff"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7%20M1;product=Objectteams"><img title="Bugs fixed for 0.7M1" src="../../images/bug.gif"></a></a></td>
                </tr>
            </table>
            <h6>Common Documentation:</h6>
            <ul>
                    <li>See this <a href="http://wiki.eclipse.org/OTHowtos/Migrating_Projects_To_0.7">HowTo</a> for migrating your projects from an older tool version (from objectteams.org)</li>
                    <li>User visible changes are collected in the cummulative <a href="../new_in_0.7.html">New & Noteworthy</a>.</li>
            </ul>
        </div>
	
	<h2>Stable Releases for Eclipse 3.3 - 3.6 M6</h2>
		<p>For older versions and until a stable release is approved by the Eclipse foundation, you may still
		consult our <a href="http://www.objectteams.org/distrib">old download page</a> at <b>www.objectteams.org</b> for existing releases.</p>
		
		
	<h2>Howto Install</h2>
	<strong>Nothing special, just...</strong>
	<ol>
	<li>Install <a href="http://archive.eclipse.org/eclipse/downloads/drops/I20100608-0911/index.php">Eclipse SDK 3.6.0 RC4</a> (you may also use your favorite Eclipse Package but it has to be exactly 3.6.0 RC4).</li>
	<li>Open "<code>Help > Install New Software ...</code>"</li> 
	<li>Add <a href="http://archive.eclipse.org/objectteams/updates/0.7">http://archive.eclipse.org/objectteams/updates/0.7</a> as a new site to work with</li>
	<li>Select both provided features
		<ul>
		<li><strong>Object Teams Development Tooling (Incubation)</strong>: the IDE for Object Teams</li>
		<li><strong>Object Teams Equinox Integration (Incubation)</strong>: runtime required by the above feature</li>
		</ul>
		or simply check the parent category <strong>"OTDT 0.7.x (Incubation) based on Eclipse 3.6"</strong>.</li>
	<li>Click through the wizard upto and including the required restart of Eclipse.</li>
	</ol>
		 
	<strong>Some hints on how you may check the installation:</strong>
	<ul>
	<li>Browse content from the welcome page or from help ("Object Teams Development User Guide").</li>
	<li>Find new icons in "About Eclipse SDK" dialog</li>
	<li>From this about dialog click through to the plug-ins of "Eclipse RCP" and scroll to the "Workbench" plug-in:</br>
	    You should see a note that this plug-in is adapted by two Object Teams plug-ins.</li>
	<li>Install a shipped OT/J example via "<code>File > New > Example ...</code>", one of
		<ul>
		<li>Stop Watch Example (easiest).</li>
		<li>Observer Example (straight forward).</li>
		<li>Flight Bonus Example (comprehensive, high density of OT/J concepts).</li>
		<li>Order System Example (comprehensive, not quite as dense).</li>
		<li>ATM Example (special demo for guard predicates).</li>
		</ul></li>
	</ul>
	
</div> <!-- midcolumn -->

<!-- remove the entire <div> tag to omit the right column!  -->
<div id="rightcolumn">
	<div class="sideitem">
		<h6>Incubation</h6>
		<p>This project is currently in its <a href="http://www.eclipse.org/projects/dev_process/validation-phase.php">Validation (Incubation) Phase</a>.</p> 
		<div style="text-align:center !important;">
			<a href="/projects/what-is-incubation.php">
				<img align="center" src="/images/egg-incubation.png" border="0" alt="Incubation" />
			</a>
		</div>
	</div>
    <div class="sideitem">
        <h6>More from www.objectteams.org</h6>
        <ul>
        <li><a href="http://www.objectteams.org/distrib/documentation/guide/webindex.html">OTDT User Guide</a> <span style="float:right;">(v1.4.0)</span></li>
        <li><a href="http://www.objectteams.org/distrib/otdt.html">Download (Helios)</a> <span style="float:right;">(v1.4.0)</span></li>
        <li><a href="http://www.objectteams.org/distrib/otdt_1.3.html">Download (Galileo)</a> <span style="float:right;">(v1.3.3)</span></li>
        </ul>
    </div>
</div>

EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>