<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2009, 2013
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	$pageTitle 		= "OT/J on Java 8";
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/objectteams/css/style.css"/>');
	$App->AddExtraHtmlHeader('<style type="text/css">.col-md-4 { width: 20%; !important; } div#novaContent { width: auto; !important; }</style>');
	

	# add a complete nav definition:
	# $Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=tools.objectteams", "", 1  );
	# $Nav->addNavSeparator("Object Teams", 	"/objectteams");
	# $Nav->addCustomNav("Download", "/objectteams/download.php", "_self", 2);
	# $Nav->addCustomNav("Releases 2.2.x", "#R22", "_self", 3);
	# $Nav->addCustomNav("How To Install", "#howto", 3);
    # $Nav->addCustomNav("Previous Releases", "#previous", 3);
    # $Nav->addCustomNav("Retention Policy", "#retention", 3);
    # "#R22" Milestone releases towards 2.2 Eclipse 4.3Mx)
	
	# $Nav->addCustomNav("Documentation", "/objectteams/documentation.php", "_self", 2);
	# $Nav->addCustomNav("Support", "/objectteams/support.php", "_self", 2);
	# $Nav->addCustomNav("Getting Involved", "/objectteams/getting_involved.php", "_self", 2);
	# $Nav->addCustomNav("Project Plan", "https://www.eclipse.org/projects/project-plan.php?projectid=tools.objectteams", "_self", 2);
	
	$Nav->addNavSeparator("On this page", "/objectteams/download.php", "_self");
	$Nav->addCustomNav("Releases 2.5.x", "#R25", "_self", 3);
	$Nav->addCustomNav("How To Install", "#howto", 3);
    $Nav->addCustomNav("Previous&nbsp;Releases", "#previous", 3);
    $Nav->addCustomNav("Retention Policy", "#retention", 3);
	
	$Nav->addNavSeparator("Documentation Quicklinks", "");
	$Nav->addCustomNav("Quick-Start", 	"https://wiki.eclipse.org/Object_Teams_Quick-Start", "_blank", 2);
	$Nav->addCustomNav("User Guide", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/develop.html", "_blank", 2);
	$Nav->addCustomNav("Language&nbsp;Definition", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/otjld/def/index.html", "_blank", 2);

	$html  = <<<EOHTML
<div id="midcolumn" style="
	background-image:url(../images/OTJ8-concepts.png);background-repeat:no-repeat;
	background-position:35px 40px;width:795px;height:720px;">
	<h1 style="position:relative; top:0px;height:36px; text-align:center;">OT/J on Java 8 is …</h1>
	<h2 style="position:relative; left:170px; top:0px;height:24px;">Composition:</h2>
	<div style="position:relative; left:30px; top:22px; width:380px;height:300px;font-size:16px;">
		The essence of object-oriented programming is to compose a system from a possibly large number of interacting classes and objects.
		<p>
		Since version 1.1, <b class="orange">Java</b> has not seen any enhancements for this task.
		</p>
		<p style="margin:-5px 0px 0px;">
		<a class="orange" href="http://wiki.eclipse.org/OTJ">OT/J</a> adds three closely related concepts:
		</p>
		<ul style="margin:0px;">
		<li style="margin:3px;"><a href="http://wiki.eclipse.org/OTJ_Primer/Role_Playing">Roles</a> can be attached to a base object for <b>dynamic specialization</b>.</li>
		<li style="margin:-5px 0px 0px;">Roles are grouped into a <a href="http://wiki.eclipse.org/OTJ_Primer/Role_Containment">Team</a> to yield <b>cohesive modules</b> for use-cases &amp; features.</li>
		<li style="margin:-5px 0px 0px;">Teams support nested inheritance for <b>large-scale specialization</b>.</li></ul>
	</div>
	<h2 style="position:relative; left:80px; top:-420px;
		display:block;transform:rotate(-90deg);
		-ms-transform:rotate(-90deg);
		-webkit-transform:rotate(-90deg);">Algorithms:</h2>
	<div style="position:relative; left:520px; top:-195px; width:245px;font-size:16px;">
		<a class="orange" href="http://wiki.eclipse.org/OTJ">OT/J</a> is agnostic to how algorithms are encoded (programming in the small),<br/>
		but you may leverage lambda expressions from <b class="orange">Java 8</b> in your OT/J programs for that touch
		of <b>functional programming</b>.
	</div>
	<h2 style="position:relative; left:156px; top:-140px;">	Types:</h2>
	<div style="position:relative; left:270px; top:-185px; width:480px;font-size:16px;">
		A rich type system is key for attaching <b>semantics</b> to the elements of your program.
		<ul>
			<li style="margin-left:-12px;"><b class="orange">Java 8</b> features …<br/>
			<b>type annotations</b> for expressing more properties of types,</li>
			<li style="margin-left:-12px;"><a class="orange" href="http://help.eclipse.org/luna/index.jsp?topic=%2Forg.eclipse.jdt.doc.user%2FwhatsNew%2Fjdt_whatsnew.html&cp=1_6_2&anchor=null-types-annotations">JDT</a> leverages  …<br/>
			type annotations for detecting <b>null</b> related errors,</li>
			<li style="margin-left:-12px;"><a class="orange" href="http://wiki.eclipse.org/OTJ">OT/J</a> introduces …<br/>
			dependent types to express object <b>ownership</b>.</li>
		</ul>
	</div>
</div>

EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>