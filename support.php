<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Object Teams support";
	$pageKeywords	= "ObjectTeams, support, forum, bugtracker";
	$pageAuthor		= "Stephan Herrmann";
		
	# Paste your HTML content between the EOHTML markers!	
	ob_start();
	?>
	<div id="midcolumn">
		<h1>Support for Object Teams</h1>
		
		<h2>Forum</h2>
		<p>
			Should you have any questions regarding Object Teams the primary place to ask is the
			<a href="http://www.eclipse.org/forums/eclipse.objectteams">Object Teams forum</a>.
		</p>

		<h2>Bug Tracking</h2>
		<p>
			When searching existing bugs or reporting new bugs against any Object Teams component please select:
			<table style="margin-left:15px;" width="100%">
			<tr><td width="20%"><b>Classification:</b></td>
				<td width="80%">Tools</td></tr>
			<tr><td><b>Product:</br></td>
				<td>Objectteams</td></tr>
			<tr><td><b>Component:</b></td>
				<td>Any of OTDT, OTEquinox, OTJ, OTJPA or Releng</td></tr>
			</table>
			<p style="margin-top:10px;">
			You may use these links as shortcuts:
			</p>
			<ul>
			<li><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;bug_status=UNCONFIRMED;bug_status=NEW;bug_status=ASSIGNED;bug_status=REOPENED;product=Objectteams">All open bugs for Object Teams</a></li>
			<!--li><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools;query_format=advanced;target_milestone=0.7.1;product=Objectteams">All bugs targeted for the next Object Teams milestone (0.7.1)</a></li-->
			<li><a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Objectteams">File a new bug against Object Teams</a></li>
			</ul> 
  		</p>
  		<p>
			Please use the components as follows:
			<table style="margin-left:15px;" width="100%">
			<tr><td valign="top" width="20%"><b>OTJ</b></td><td width="80%">Problems relating to the language implemention of OT/J, such as incorrect compiler messages, wrong byte code, unexpected runtime behaviour</td></tr>
			<tr><td valign="top"><b>OTDT</b></td><td>Problems relating to any aspect of the OT/J IDE (editing, views, debugging etc.).</td></tr>
			<tr><td valign="top"><b>OTEquinox</b></td><td>Problems relating to executing plugins written in OT/J.</td></tr>
			<tr><td valign="top"><b>OTJPA</b></td><td>Problems relating to persisting OT/J programs using EclipseLink (JPA)</td></tr>
			</table>
		</p>
	</div> <!-- midcolumn -->

	<!-- remove the entire <div> tag to omit the right column!  -->
	<!--div id="rightcolumn">
		<div class="sideitem">
			<h6>Related Links</h6>
			<ul>
				<li><a href="midcolumn.php">#midcolumn Template</a></li>
				<li><a href="fullcolumn.php">#fullcolumn Template</a></li>
			</ul>
		</div>
		<div class="sideitem">
			<h6>&lt;h6&gt; tag</h6>
				<div class="modal">
					Wrapping your content using a div.modal tag here will add the gradient background
				</div>
		</div>
	</div-->

	
	<?
	$html = ob_get_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>