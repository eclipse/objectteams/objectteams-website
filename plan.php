<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Object Teams plan";
	$pageKeywords	= "ObjectTeams";
	$pageAuthor		= "Stephan Herrmann";
		
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="css/plan.css"/>');

	# Paste your HTML content between the EOHTML markers!	
	ob_start();
?>
	<div id="midcolumn">
		<h1><?= $pageTitle ?></h1>
		Content was moved <a href="http://www.eclipse.org/projects/project-plan.php?projectid=tools.objectteams">here</a>. 
	</div>
<?
	$html = ob_get_clean();

	# Generate the web page
	$App->generatePage('Nova', $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
