<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2009, 2013
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	$pageTitle 		= "Object Teams - Download";
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/objectteams/css/style.css"/>');
	$App->AddExtraHtmlHeader('<style type="text/css">.col-md-4 { width: 20%; !important; }</style>');
	

	# add a complete nav definition:
	# $Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=tools.objectteams", "", 1  );
	# $Nav->addNavSeparator("Object Teams", 	"/objectteams");
	# $Nav->addCustomNav("Download", "/objectteams/download.php", "_self", 2);
	# $Nav->addCustomNav("Releases 2.2.x", "#R22", "_self", 3);
	# $Nav->addCustomNav("How To Install", "#howto", 3);
    # $Nav->addCustomNav("Previous Releases", "#previous", 3);
    # $Nav->addCustomNav("Retention Policy", "#retention", 3);
    # "#R22" Milestone releases towards 2.2 Eclipse 4.3Mx)
	
	# $Nav->addCustomNav("Documentation", "/objectteams/documentation.php", "_self", 2);
	# $Nav->addCustomNav("Support", "/objectteams/support.php", "_self", 2);
	# $Nav->addCustomNav("Getting Involved", "/objectteams/getting_involved.php", "_self", 2);
	# $Nav->addCustomNav("Project Plan", "https://www.eclipse.org/projects/project-plan.php?projectid=tools.objectteams", "_self", 2);
	
	$Nav->addNavSeparator("On this page", "/objectteams/download.php", "_self");
	$Nav->addCustomNav("Releases 2.5.x", "#R25", "_self", 3);
	$Nav->addCustomNav("How To Install", "#howto", 3);
    $Nav->addCustomNav("Previous&nbsp;Releases", "#previous", 3);
    $Nav->addCustomNav("Retention Policy", "#retention", 3);
	
	$Nav->addNavSeparator("Documentation Quicklinks", "");
	$Nav->addCustomNav("Quick-Start", 	"https://wiki.eclipse.org/Object_Teams_Quick-Start", "_blank", 2);
	$Nav->addCustomNav("User Guide", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/develop.html", "_blank", 2);
	$Nav->addCustomNav("Language&nbsp;Definition", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/otjld/def/index.html", "_blank", 2);

	$html  = <<<EOHTML
<div id="midcolumn" style="width:80%; !important;">
	<h1>$pageTitle</h1>
	<p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a> unless otherwise specified.</p>
	
		<ul class="novaArrow">
        <li><a href="features.php">Features of the Object Teams Development Tooling (OTDT)</a></li>
        <li><a href="https://wiki.eclipse.org/images/f/fc/OTDT_Overview.png">Screenshot of the OTDT in action</a></li>
        </ul>


    <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
        <tr>
            <td><img src="images/Idea.png"></td>
            <td style="width:100%;padding:8px;">
                Between releases Indigo (2011) and 2020-06, Object Teams was a member of the Simultaneous Release Train.
                For these versions no further URL must be configured for installing the OTDT and/or OT/Equinox.
                Simply select the update site corresponding to the release at https://download.eclipse.org/releases/<i>version name</i></a> 
                and open the <b>Programming Languages</b> category.
                <br>
                Newer releases of Object Teams are distributed separately via the <a href="https://marketplace.eclipse.org/content/object-teams-development-tooling">market place entry</a> 
                and the update sites listed below. 
            </td>
        </tr>
    </table>

    <h2><a name="R28"></a>Recent Builds</h2>

        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Integration builds towards OTDT 2.8.2</h3>
            <table width="100%" id="DL282">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell" style="padding-left:10px;">Matching Eclipse Version</th>
						<th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202112010822">Build 202112010822 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2021-12/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2021-12</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    &nbsp;<a href="https://www.eclipse.org/forums/index.php/t/1109436/"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2021-12&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202109181256">Build 202109181256 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2021-09/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2021-09</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    <!--&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>-->
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2021-09&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202106182116">Build 202106182116 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2021-06/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2021-06</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    <!--&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>-->
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2021-06&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202103070142">Build 202103070142 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2021-03/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2021-03</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    <!--&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>-->
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2021-03&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202012220836">Build 202012220836 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2020-12/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2020-12</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    <!--&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>-->
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&list_id=20245934&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2020-12&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong><a href="https://download.eclipse.org/objectteams/updates/ot2.8/202009051622">Build 202009051622 Repo</strong></td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://www.eclipse.org/downloads/packages/release/2020-09/r"><img src="images/arrow.gif">&nbsp;Eclipse&nbsp;2020-09</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">
                    <!--<a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a>-->
                    </td>
                    <td class="dl-cell">
                    <!--&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>-->
                    </td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?classification=Tools&list_id=20245934&product=Objectteams&query_format=advanced&resolution=FIXED&status_whiteboard=2020-09&status_whiteboard_type=allwordssubstr"><img title="Bugs fixed" src="images/bug.gif"></a></td>
                </tr>
			</tbody></table>
        </div>

	<h2><a name="howto"></a>Howto Install</h2>
	<strong>Nothing special, just...</strong>
	<ol>
	<li>Install the Eclipse package of your choosing, preferably one that already contains the Java Development Tools</li>
	<li>Open "<code>Help > Install New Software ...</code>"</li> 
	<li>Choose the software site according to the above table and open the <b>Programming Languages</b> category.</li>
	<li>Select these features
		<ul>
		<li><strong>Object Teams Development Tooling</strong>: the IDE for Object Teams</li>
		<li><strong>Object Teams Equinox Integration</strong>: runtime required by the above feature</li>
		<li><em>Optionally:</em> Object Teams Equinox Integration - Turbo Add-On: <a title="aspect permission negotiation &amp; forced exports">special features of OT/Equinox</a></li>
		</ul></li>
	<li><i>Recommended: un-check "Contact all update sites during install to find required software"</i></li>
	<li>Click through the wizard upto </li>
	<li><strong><i>Instead of the recommended restart,</i> cleanly <span style="color:red;">exit</span> Eclipse.</strong></li>
	<li><strong>Freshly <span style="color:green;">start</span> Eclipse</strong> from commandline or desktop (this last step is needed to re-read a configuration changed during installation.</li>
	</ol>
		 
	<strong>Some hints on how you may check the installation:</strong>
	<ul>
	<li>Browse content from the welcome page or from help ("Object Teams Development User Guide").</li>
	<li>Find new icons in "About Eclipse SDK" dialog</li>
	<li>From this about dialog click through to the plug-ins of "Eclipse RCP" and scroll to the "Workbench" plug-in:</br>
	    You should see a note that this plug-in is adapted by two Object Teams plug-ins.</li>
	<li>Install a shipped OT/J example via "<code>File > New > Example ...</code>", one of
		<ul>
		<li>Stop Watch Example (easiest).</li>
		<li>Observer Example (straight forward).</li>
		<li>Flight Bonus Example (comprehensive, high density of OT/J concepts).</li>
		<li>Order System Example (comprehensive, not quite as dense).</li>
		<li>ATM Example (special demo for guard predicates).</li>
		</ul></li>
	</ul>

	<h2><a name="previous"></a>Previous Releases for Eclipse 3.3 - 2020-06</h2>
		<ul>
		<li><a href="download-prev.php">Release up-to 2.8.1 for Eclipse 2020-06</a>
		<li>For still older versions, you may still
		consult our <a href="http://www.objectteams.org/distrib/otdt.html" target=_blank>old download page</a> at <b>www.objectteams.org</b> for existing releases.</li>		
		</ul>
	<h2><a name="retention"></a>Retention Policy</h2>
	<ul>
	<li>Official releases (incl. service releases) will never be removed from the server<br/>(old versions may be moved to the archive site, though)</li>
	<li>Milestone builds will be kept until the final release</li>
	<li>No such promises are made for any intermediate ("unstable") versions</li>
	</ul> 
	
</div> <!-- midcolumn -->


EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
