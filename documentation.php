<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2010, 2014 Eclipse Foundation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Object Teams documentation";
	$pageKeywords	= "ObjectTeams, documentation";
	$pageAuthor		= "Stephan Herrmann";
		
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	$Nav->addNavSeparator("Background Documentation", 	"http://www.objectteams.org", _blank);
	$Nav->addCustomNav("Introduction", 	"http://www.objectteams.org/intro.html", _blank, 2);
	$Nav->addCustomNav("Publications", 	"http://www.objectteams.org/publications", _blank, 2);
	$Nav->addCustomNav("Presentations", 	"http://www.objectteams.org/publications/presentations.html", _blank, 2);
	
	ob_start();
	?>
	<div id="midcolumn">
		<!-- This is the staging branch -->
		<h1>Documentation for Object Teams</h1>
        <div style="background-color:#D0D0D0;padding:1px;">
            <table style="background-color:#def4fe;margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding-left:6px;"><a href="http://wiki.eclipse.org/Object_Teams_Quick-Start"><b>Object Teams Quick-Start</b></a><br /> 
					For the "impatient":<br />
                    Installing the tool and writing your first application / bundle in < 15 min.
                </td>
            </tr>
            </table>
        </div>
		<h2>General Introductions</h2>
		<p>
			<ul>
			<li><a href="http://wiki.eclipse.org/OTJ">Introduction to OT/J</a> &mdash; the language</li> 
			<li><a href="http://wiki.eclipse.org/Object_Teams_Development_Tooling">Object Teams Development Tooling (OTDT)</a> &mdash; the IDE
				<ul><li><a href="http://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/develop.html">User guide</a></li></ul></li> 
			<li><a href="http://wiki.eclipse.org/Category:OTEquinox">OT/Equinox</a> &mdash; the module system</li>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams">List of all Wiki pages</a> on Object Teams</li>
			</ul>
		</p>
		
		<h2>Language Definition</h2>
		<div>
			The <b>OT/J Language Definition (OTJLD)</b>, <i>the</i> definitive reference for OT/J is still hosted on www.objectteams.org:
			<ul style="margin-top:0px;">
				<li>Most recent version:
					<ul style="padding-bottom:0px;">
					<li><a href="http://www.objectteams.org/def/1.3.1/index.html" target=_blank>OT/J Language Definition (OTJLD) – version 1.3.1</a></li>
	 				<li>Updates after the 1.3 freeze (May 2011):
	 				    &nbsp;&nbsp;&nbsp;&bull;&nbsp;<a href="http://www.objectteams.org/def/1.3.1/sB.html#sB.2.4" target=_blank>additions</a>
	 				</li>
                    <li><a href="http://www.objectteams.org/def/OTJLDv1.3.1-current.pdf" target=_blank>Print version (PDF)<img valign="middle" src="/mylyn/images/icon-pdf.gif"/></a>
                    </li>
                    </ul>
				<li>Current stable version:
					<ul style="padding-bottom:0px;">
					<li><a href="http://www.objectteams.org/def/1.3/index.html" target=_blank>OT/J Language Definition (OTJLD) – version 1.3</a></li>
	 				<li>Updates after the 1.2 freeze (June 2009):
	 				    &nbsp;&nbsp;&bull;&nbsp;<a href="http://www.objectteams.org/def/1.3/sB.html#sB.1.3" target=_blank>changes</a>
	 				    &nbsp;&nbsp;&nbsp;&bull;&nbsp;<a href="http://www.objectteams.org/def/1.3/sB.html#sB.2.3" target=_blank>additions</a>
	 				</li>
                    <li><a href="http://www.objectteams.org/def/OTJLDv1.3-final.pdf" target=_blank>Print version (PDF)<img valign="middle" src="/mylyn/images/icon-pdf.gif"/></a>
                    </li>
                    </ul>
				</li>
				<li style="padding:0px;"><a href="http://www.objectteams.org/documentation.html" target=_blank>More versions</a></li>
			</ul>
		</div>
		
		<h2>Examples &amp; Patterns</h2>
		<p>
			Source code examples for OT/J are collected in the <a href="http://wiki.eclipse.org/Category:Object_Teams">Wiki</a>
			<ul>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams_Examples">Category:Object Teams Examples</a>.<br/>
				<span style="text-decoration:underline;margin-top:5px;">Levels:</span> ranging from introductory to advanced.</li>
			<li><a href="http://wiki.eclipse.org/Category:Object_Teams_Patterns">Category:Object Teams Patterns</a>.</li>
			</ul>
		</p>
		
		<h2>Object Teams Development Tooling</h2>
		<ul>
        	<li><a href="features.php">Features of the OTDT</a></li>
        </ul>
		
		
	</div> <!-- midcolumn -->

	
	<?
	$html = ob_get_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>