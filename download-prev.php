<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
/*******************************************************************************
 * Copyright (c) 2009, 2013
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *    Stephan Herrmann
 *******************************************************************************/

	$pageTitle 		= "Object Teams - Download of Previous Releases";
	$App->AddExtraHtmlHeader('<link rel="stylesheet" type="text/css" href="/objectteams/css/style.css"/>');
	$App->AddExtraHtmlHeader('<style type="text/css">.col-md-4 { width: 20%; !important; }</style>');
	
	$Nav->addNavSeparator("On this page", "/objectteams/download-prev.php", "_self");
	$Nav->addCustomNav("Releases 2.4.x", "#R24", "_self", 3);
	$Nav->addCustomNav("Releases 2.3.x", "#R23", "_self", 3);
	$Nav->addCustomNav("Releases 2.2.x", "#R22", "_self", 3);
	$Nav->addCustomNav("Releases 2.1.x", "#R21", "_self", 3);
	$Nav->addCustomNav("Releases 2.0.x", "#R20", "_self", 3);
	$Nav->addCustomNav("Releases 0.7.x", "#R07", "_self", 3);
	$Nav->addCustomNav("How To Install", "#howto", 3);
	$Nav->addCustomNav("Even&nbsp;Older", "#previous", 3);
	$Nav->addCustomNav("Retention Policy", "#retention", 3);
	
	$Nav->addNavSeparator("Documentation Quicklinks", "");
	$Nav->addCustomNav("Quick-Start", 	"https://wiki.eclipse.org/Object_Teams_Quick-Start", "_blank", 2);
	$Nav->addCustomNav("User Guide", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/develop.html", "_blank", 2);
	$Nav->addCustomNav("Language&nbsp;Definition", 	"https://help.eclipse.org/topic/org.eclipse.objectteams.otdt.doc/guide/otjld/def/index.html", "_blank", 2);

	
	$html  = <<<EOHTML
<div id="midcolumn" style="width:80%; !important;">
	<h1>$pageTitle</h1>
	<p>All downloads are provided under the terms and conditions of the <a href="/legal/epl/notice.php">Eclipse Foundation Software User Agreement</a> unless otherwise specified.</p>
	
		<ul class="novaArrow">
        <li><a href="features.php">Features of the Object Teams Development Tooling (OTDT)</a></li>
        </ul>

        	<h2><a name="R25"></a>Stable Release 2.5.x for Eclipse 4.6 (Neon)</h2>
        <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
               		Object Teams is part of the <b><a href="https://www.eclipse.org/neon">Neon simultaneous release</a></b>.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/neon">Neon - https://download.eclipse.org/releases/neon</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
          	</tr>
        </table>

        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/ot2.5"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.5.x  can be found at
			<a href="https://download.eclipse.org/objectteams/updates/ot2.5">https://download.eclipse.org/objectteams/updates/ot2.5</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Release 2.5:</h3>
            <table width="100%" id="DL25">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell" style="padding-left:10px;">Required Eclipse Version</th>
						<th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.5.3 Update Release</strong> &mdash; (2017/03/23)<br/>
	            	<img src="images/otdt/warning_obj@2x.png"/><b><font color="red">Note:</font> final version 2.5.3 is available <em>only</em> from 
	            		<a href="http://download.eclipse.org/objectteams/updates/ot2.5/201703071318">http://download.eclipse.org/objectteams/updates/ot2.5 
	            		<img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" width="32" height="32" alt="Download"/></a></b></td>
    		</tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="http://download.eclipse.org/eclipse/downloads/drops4/R-4.6.3-201703010400/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.6.3</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.3-201703071315.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&classification=Tools&query_format=advanced&product=Objectteams&resolution=FIXED&target_milestone=2.5.3"><img title="Bugs fixed for 2.5.3" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.5.2 Update Release</strong> &mdash; (2016/12/21)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="http://download.eclipse.org/eclipse/downloads/drops4/R-4.6.2-201611241400/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.6.2</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.2-201612071654.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&classification=Tools&query_format=advanced&product=Objectteams&resolution=FIXED&target_milestone=2.5.2"><img title="Bugs fixed for 2.5.2" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.5.1 Update Release</strong> &mdash; (2016/09/28)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="http://download.eclipse.org/eclipse/downloads/drops4/R-4.6.1-201609071200/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.6.1</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;">&nbsp;</td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&classification=Tools&query_format=advanced&product=Objectteams&resolution=FIXED&target_milestone=2.5.1"><img title="Bugs fixed for 2.5.1" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.5.0 Final Release</strong> &mdash; (2016/06/22)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.6-201606061100/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.6</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.5.0-201606070953.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;<a href="https://www.eclipse.org/objectteams/pages/new_in_2.5.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&classification=Tools&query_format=advanced&product=Objectteams&resolution=FIXED&target_milestone=2.5&target_milestone=2.5%20M1&target_milestone=2.5%20M2&target_milestone=2.5%20M3&target_milestone=2.5%20M4&target_milestone=2.5%20M5&target_milestone=2.5%20M6&target_milestone=2.5%20M7&target_milestone=2.5%20RC"><img title="Bugs fixed for 2.5" src="images/bug.gif"></a></td>
                </tr>
			</tbody></table>
        </div>

	<h2><a name="R24"></a>Stable Release 2.4.x for Eclipse 4.5 (Mars)</h2>
        <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
               		Object Teams is part of the <b><a href="https://www.eclipse.org/mars">Mars simultaneous release</a></b>.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/mars">Mars - https://download.eclipse.org/releases/mars</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
          	</tr>
        </table>

        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/ot2.4"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.4.x  can be found at
			<a href="https://download.eclipse.org/objectteams/updates/ot2.4">https://download.eclipse.org/objectteams/updates/ot2.4</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Release 2.4:</h3>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell" style="padding-left:10px;">Required Eclipse Version</th>
						<th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.4.0 Final Release</strong> &mdash; (2015/06/24)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.5-201506032000/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.5</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.4.0-201506091714.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;<!--<a href="https://www.eclipse.org/objectteams/pages/new_in_2.3.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a>--></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&classification=Tools&query_format=advanced&product=Objectteams&target_milestone=2.4%20M1&target_milestone=2.4%20M2&target_milestone=2.4%20M3&target_milestone=2.4%20M4&target_milestone=2.4%20M5&target_milestone=2.4%20M6&target_milestone=2.4%20M7&target_milestone=2.4%20RC"><img title="Bugs fixed for 2.4" src="images/bug.gif"></a></td>
                </tr>
			</tbody></table>
        </div>

	<h2><a name="R23"></a>Stable Release 2.3.x for Eclipse 4.4 (Luna)</h2>
        <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
               		Object Teams participated the <b><a href="https://www.eclipse.org/luna">Luna simultaneous release</a></b>.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/luna">Luna - https://download.eclipse.org/releases/luna</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
          	</tr>
        </table>

        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/ot2.3"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.3.x  can be found at
			<a href="https://download.eclipse.org/objectteams/updates/ot2.3">https://download.eclipse.org/objectteams/updates/ot2.3</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Release 2.3:</h3>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell" style="padding-left:10px;">Required Eclipse Version</th>
						<th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.3.2 Service Release</strong> &mdash; (2015/02/27)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.4.2-201502041700/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.4.2</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.3.2-201502101145.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&list_id=5963256&classification=Tools&query_format=advanced&product=Objectteams&target_milestone=2.3.2&target_milestone=2.3.1"><img title="Bugs fixed for 2.3.1 &amp; 2.3.2" src="images/bug.gif"></a></td>
                </tr>
			<tr><td colspan="5" class="release-cell"><strong>OTDT 2.3.0 Final Release</strong> &mdash; (2014/06/25)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.4-201406061215/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.4</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.3.0-201406101336.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.3.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&list_id=5963256&classification=Tools&query_format=advanced&product=Objectteams&target_milestone=2.3%20M1&target_milestone=2.3%20M2&target_milestone=2.3%20M3&target_milestone=2.3%20M4&target_milestone=2.3%20M5&target_milestone=2.3%20M6&target_milestone=2.3%20M7&target_milestone=2.3%20RC1&target_milestone=2.3%20RC2&target_milestone=2.3&target_milestone=2.2%20J8"><img title="Bugs fixed for 2.3" src="images/bug.gif"></a></td>
                </tr>
			</tbody></table>
        </div>	

	<h2><a name="R22"></a>Stable Release 2.2.x for Eclipse 4.3 (Kepler)</h2>
        <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
               		Object Teams participated in the <b><a href="https://www.eclipse.org/kepler">Kepler simultaneous release</a></b> train.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/kepler">Kepler - https://download.eclipse.org/releases/kepler</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
          	</tr>
        </table>

        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/ot2.2"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.2.x  can be found at
			<a href="https://download.eclipse.org/objectteams/updates/ot2.2">https://download.eclipse.org/objectteams/updates/ot2.2</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Release 2.2:</h3>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell" style="padding-left:10px;">Required Eclipse Version</th>
						<th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.2.1 Service Release 1</strong> &mdash; (2013/09/27)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.3.1-201309111000/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.3.1</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.2.1-201309101915.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&list_id=5963256&classification=Tools&query_format=advanced&product=Objectteams&target_milestone=2.2.1"><img title="Bugs fixed for 2.2.1" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.2.0 Final Release</strong> &mdash; (2013/06/26)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell" style="padding-left:5px;"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.3-201306052000/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.3</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.2.0-201306071800.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.2.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?j_top=OR&list_id=5963256&classification=Tools&query_format=advanced&product=Objectteams&target_milestone=2.2%20M1&target_milestone=2.2%20M2&target_milestone=2.2%20M3&target_milestone=2.2%20M4&target_milestone=2.2%20M5&target_milestone=2.2%20M6&target_milestone=2.2%20M7&target_milestone=2.2%20RC1&target_milestone=2.2%20RC2&target_milestone=2.2"><img title="Bugs fixed for 2.2" src="images/bug.gif"></a></td>
                </tr>
			</tbody></table>
        </div>
			
	<h2><a name="R21"></a>Stable Release 2.1.x for Eclipse 4.2/3.8 (Juno)</h2>
        <table class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
			   		Object Teams participated in the <b><a href="https://www.eclipse.org/juno">Juno simultaneous release</a></b> train.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/juno">Juno - https://download.eclipse.org/releases/juno</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
            </tr>
        </table>

        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/ot2.1"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.1.x  can be found at
			<a href="https://download.eclipse.org/objectteams/updates/ot2.1">https://download.eclipse.org/objectteams/updates/ot2.1</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Releases 2.1.x:</h3>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell">Required Eclipse Version</th><th class="dl-cell">Command Line Compiler</th>
							<th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.1.2 Service Release</strong> &mdash; (2013/03/01)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.2.2-201302041200/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.2.2</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-M-2.1.2-201301271215.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell">&nbsp;</td>
                </tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.1.1 Service Release</strong> &mdash; (2012/09/29)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.2.1-201209141800/"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.2.1</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-S-2.1.1-201209011843.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell">&nbsp;</td>
                </tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.1.0 Final Release</strong> &mdash; (2012/06/27)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops4/R-4.2-201206081400/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;4.2</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.1.0-201206090440.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.1.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?list_id=2099361;resolution=FIXED;classification=Tools;query_format=advanced;target_milestone=2.1%20M1;target_milestone=2.1%20M2;target_milestone=2.1%20M3;target_milestone=2.1%20M4;target_milestone=2.1%20M5;target_milestone=2.1%20M6;target_milestone=2.1%20M7;target_milestone=2.1%20RC1;target_milestone=2.1%20RC2;target_milestone=2.1%20RC3;target_milestone=2.1%20RC4;target_milestone=2.1;product=Objectteams"><img title="Bugs fixed for 2.1" src="images/bug.gif"></a></td>
                </tr>
            </tbody></table>
        </div>

	<h2><a name="R20"></a>Stable Release 2.0.x for Eclipse 3.7 (Indigo)</h2>
        <table  class="btn-warning orange-box" style="margin:0px;padding:4px;width:100%">
            <tr>
                <td><img src="images/Idea.png"></td>
                <td style="width:100%;padding:8px;">
               		Object Teams participated in the <b>Indigo simultaneous release</b> train.
					This means, no further URL must be configured for installing the OTDT and/or OT/Equinox,
					simply select the <a href="https://download.eclipse.org/releases/indigo">Indigo - https://download.eclipse.org/releases/indigo</a> 
					software site and open the <b>Programming Languages</b> category.
                </td>
            </tr>
        </table>
        <blockquote style="margin:20px;"><b>Version numbers:</b><em>Before moving to Eclipse.org the project shipped releases 
				up-to version 1.4.0 from <a href="http://www.objectteams.org/distrib/otdt.html" target=_blank>objectteams.org</a>. 
				Then, during the "incubation" phase at Eclipse.org the project was obliged to use version numbers below 1.0,
				thus we released 0.7.0 - 0.7.2 (Incubation) and some milestones 0.8 Mx (Incubation).
				With the project <b>graduation</b> on June 8, 2011, we switched to <b>version 2.0.0</b>,
				indicating the advance over the aforementioned release 1.4.0.</em>	 
			</blockquote>
        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/2.0"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;">A specific repository containing just Object Teams 2.0.x can be found at
			<a href="https://download.eclipse.org/objectteams/updates/2.0">https://download.eclipse.org/objectteams/updates/2.0</a>
                </td>
            </tr>
        </tbody></table>
        <div style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Releases 2.0.x:</h3>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell">Required Eclipse Version</th><th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.0.2 (Service Release 2)</strong> &mdash; (2012/02/24)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.7.2-201202080800/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;3.7.2</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.0.2-201201310613.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;target_milestone=2.0.2;product=Objectteams;classification=Tools"><img title="Bugs fixed for 2.0.2" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.0.1 (Service Release 1)</strong> &mdash; (2011/09/23)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.7.1-201109091335/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;3.7.1</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.0.1-201109101022.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell">&nbsp;</td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;target_milestone=2.0.1;product=Objectteams;classification=Tools"><img title="Bugs fixed for 2.0.1" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan="5" class="release-cell"><strong>OTDT 2.0.0 Final Release</strong> &mdash; (2011/06/22)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.7-201106131736/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;3.7</a></td>
                    <td class="dl-cell" style="min-width: 110px;padding-left:5px;"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-2.0.0-201106070718.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.0.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;target_milestone=0.8%20M3;target_milestone=0.8%20M4;target_milestone=0.8%20M5;target_milestone=0.8%20M6;target_milestone=0.8%20M7;target_milestone=0.8;target_milestone=2.0%20RC1;target_milestone=2.0%20RC2;target_milestone=2.0%20RC3;target_milestone=2.0%20RC4;target_milestone=2.0;product=Objectteams;classification=Tools"><img title="Bugs fixed for 2.0" src="images/bug.gif"></a></td>
                </tr>
            </tbody></table>
        </div>
        <!--div id="SR" style="margin-left:30px;" class="sideitem">
            <h6>Release candidates towards SR1:</h6>
            <table width="100%">
	            <tbody><tr valign="top"><th>&nbsp;</th><th class="dl-cell">Required Eclipse Version</th><th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
	            <tr><td colspan="5"><strong>OTDT 2.0.1 RC2</strong> &mdash; (2011/08/27)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/M20110825-0847/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;3.7.1RC2</a></td>
                    <td style="width: 105px;" class="dl-cell"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-S-2.0.1-201108270525.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.0.html#language"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;target_milestone=2.0.1;product=Objectteams;classification=Tools"><img title="Bugs fixed for 2.0.1" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan="5"><strong>OTDT 2.0.1 RC1</strong> &mdash; (2011/08/19)</td></tr>
                <tr><td style="width: 50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/M20110810-0800/index.php"><img src="images/arrow.gif">Eclipse&nbsp;SDK&nbsp;3.7.1RC1</a></td>
                    <td style="width: 105px;" class="dl-cell"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-I-2.0.1-201108161749.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/objectteams/pages/new_in_2.0.html#language"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;target_milestone=2.0.1;product=Objectteams;classification=Tools"><img title="Bugs fixed for 2.0.1" src="images/bug.gif"></a></td>
                </tr>
            </tbody></table>
        </div-->


	<h2><a name="R07"></a>Releases 0.7.x for Eclipse 3.6.x (Helios)</h2>
                <p>During the incubation phase of the Object Teams project, releases had to be numbered 0.7.x,
                   which are the direct successors of the stable release 1.4.0 from <a href="http://www.objectteams.org/distrib/otdt.html" target=_blank>objectteams.org</a>.</p>
        <table border="0" style="margin-left:30px;margin-top:10px;margin-bottom:10px;">
            <tbody><tr>
                <td><a href="https://download.eclipse.org/objectteams/updates/0.7"><img src="https://dev.eclipse.org/huge_icons/actions/go-bottom.png" alt="Download" align="left"></a></td>
                <td style="width: 100%; padding-left: 6px;"><b>Update site:</b> <a href="https://download.eclipse.org/objectteams/updates/0.7">https://download.eclipse.org/objectteams/updates/0.7</a>
                </td>
            </tr>
        </tbody></table>
        <div  style="margin-left:30px;" class="block-box block-box-classic">
            <h3 style="margin-bottom:2px;">Releases 0.7.x:</h3>
            <table width="100%">
	            <tr valign="top"><th>&nbsp;</th><th class="dl-cell">Required Eclipse Version</th><th class="dl-cell">Command Line Compiler</th><th class="dl-cell">New&nbsp;&amp; Noteworthy</th><th class="dl-cell">Bugs Fixed</th></tr>
	            <tr><td colspan=5 class="release-cell"><strong>OTDT 0.7.2 Release</strong> &mdash; (2011/02/25)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.6.2-201102101200/index.php"><img src="images/arrow.gif">Eclipse SDK 3.6.2</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-0.7.2-201102150409.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><em>no new features</em></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?quicksearch=330304%2C331702%2C326042"><img title="Bugs fixed for 0.7.2 (since 0.7.1)" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan=5 class="release-cell"><strong>OTDT 0.7.1 Release</strong> &mdash; (2010/09/24)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.6.1-201009090800/index.php"><img src="images/arrow.gif">Eclipse SDK 3.6.1</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-0.7.1-201009231848.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="pages/new_in_0.7.1.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;classification=Tools;product=Objectteams;target_milestone=0.7.1;"><img title="Bugs fixed for 0.7.1 (since 0.7.0)" src="images/bug.gif"></a></td>
                </tr>
	            <tr><td colspan=5 class="release-cell"><strong>OTDT 0.7.0 Release</strong> &mdash; (2010/07/07)</td></tr>
                <tr><td style="width:50px;">&nbsp;</td>
                    <td class="dl-cell"><a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.6-201006080911/index.php"><img src="images/arrow.gif">Eclipse SDK 3.6.0</a></td>
                    <td class="dl-cell"><a href="https://www.eclipse.org/downloads/download.php?file=/objectteams/ecotj/ecotj-R-0.7.0-201007050922.jar"><img src="images/otdt/jar_obj.gif" valign="middle"> ecotj.jar</a></td>
                    <td class="dl-cell"><a href="pages/new_in_0.7.html"><img src="https://www.eclipse.org/jdt/core/images/new.png"></a></td>
                    <td class="dl-cell"><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced;resolution=FIXED;classification=Tools;product=Objectteams;target_milestone=0.7%20M1;target_milestone=0.7%20M2;target_milestone=0.7%20M3;target_milestone=0.7%20M4;target_milestone=0.7"><img title="Bugs fixed for 0.7 (since 1.4)" src="images/bug.gif"></a></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
	            <tr><td colspan=4><strong><a href="pages/archive/download_pre_0.7.php">Archived Milestone Releases</a></strong> &mdash; (as of 2010/06/23)</td></tr>
            </table>
            <h6>Common Documentation:</h6>
            <ul>
                    <li>See this <a href="https://wiki.eclipse.org/OTHowtos/Migrating_Projects_To_0.7">HowTo</a> for migrating your projects from an older tool version (from objectteams.org)</li>
            </ul>
        </div>
	
	<h2><a name="howto"></a>Howto Install</h2>
	<strong>Nothing special, just...</strong> <em>(example uses 3.6.2 / 0.7.2, please insert your preferred version)</em>:
	<ol>
	<li>Install <a href="https://download.eclipse.org/eclipse/downloads/drops/R-3.6.2-201102101200/index.php">Eclipse SDK 3.6.2</a> (you may also use your favorite Eclipse Package but it has to be exactly 3.6.2 for OTDT 0.7.2).</li>
	<li>Open "<code>Help &gt; Install New Software ...</code>"</li> 
	<li>Add <a href="https://download.eclipse.org/objectteams/updates/0.7">https://download.eclipse.org/objectteams/updates/0.7</a> as a new site to work with</li>
	<li>Select both provided features
		<ul>
		<li><strong>Object Teams Development Tooling (Incubation)</strong>: the IDE for Object Teams</li>
		<li><strong>Object Teams Equinox Integration (Incubation)</strong>: runtime required by the above feature</li>
		</ul>
		or simply check the parent category <strong>"OTDT 0.7.x (Incubation) based on Eclipse 3.6"</strong>.</li>
	<li><i>Recommended: un-check "Contact all update sites during install to find required software"</i></li>
	<li>Click through the wizard upto and including the required restart of Eclipse.</li>
	</ol>
		 
	<strong>Some hints on how you may check the installation:</strong>
	<ul>
	<li>Browse content from the welcome page or from help ("Object Teams Development User Guide").</li>
	<li>Find new icons in "About Eclipse SDK" dialog</li>
	<li>From this about dialog click through to the plug-ins of "Eclipse RCP" and scroll to the "Workbench" plug-in:<br>
	    You should see a note that this plug-in is adapted by two Object Teams plug-ins.</li>
	<li>Install a shipped OT/J example via "<code>File &gt; New &gt; Example ...</code>", one of
		<ul>
		<li>Stop Watch Example (easiest).</li>
		<li>Observer Example (straight forward).</li>
		<li>Flight Bonus Example (comprehensive, high density of OT/J concepts).</li>
		<li>Order System Example (comprehensive, not quite as dense).</li>
		<li>ATM Example (special demo for guard predicates).</li>
		</ul></li>
	</ul>

	<h2><a name="previous"></a>Previous Releases for Eclipse 3.3 - 3.6 M6</h2>
		<p>For older versions, you may still
		consult our <a href="http://www.objectteams.org/distrib/otdt.html" target=_blank>old download page</a> at <b>www.objectteams.org</b> for existing releases.</p>		

	<h2><a name="retention"></a>Retention Policy</h2>
	<ul>
	<li>Official releases (incl. service releases) will never be removed from the server<br>(old versions may be moved to the archive site, though)</li>
	<li>Milestone builds will be kept until the final release</li>
	<li>No such promises are made for any intermediate ("unstable") versions</li>
	</ul> 
	
</div> <!-- midcolumn -->

EOHTML;
	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
