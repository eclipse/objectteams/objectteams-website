package embedding.custom.ast;

import org.eclipse.jdt.core.compiler.CharOperation;
import org.eclipse.jdt.internal.compiler.ast.IntLiteral;
import org.eclipse.jdt.internal.compiler.impl.Constant;
import org.eclipse.jdt.internal.compiler.impl.IntConstant;
import org.eclipse.jdt.internal.compiler.lookup.BlockScope;
import org.eclipse.jdt.internal.compiler.lookup.TypeBinding;
import org.eclipse.jdt.internal.compiler.parser.TerminalTokens;

/**
 * Compiler AST representation of our new syntax element.
 */
@SuppressWarnings("restriction")
public class CustomIntLiteral extends IntLiteral 
{
	static char[][][] numbers = new char[][][]{
			{ 
				"english".toCharArray()
			}, 
			{
				"zero".toCharArray(),
				"one".toCharArray(),
				"two".toCharArray()
			},
			{ 
				"german".toCharArray()
			}, 
			{
				"null".toCharArray(),
				"eins".toCharArray(),
				"zwei".toCharArray()
			}
		};

	// example additional property of this node:
	public String language;

	public int innerSourceStart;

	public int innerSourceEnd;
	
	public CustomIntLiteral(char[] token, int outerSourceStart, int outerSourceEnd, int innerSourceStart, int innerSourceEnd) {
		super(token, outerSourceStart, outerSourceEnd);
		this.language = "english";
		this.innerSourceStart = innerSourceStart;
		this.innerSourceEnd = innerSourceEnd;
	}
	@Override
	public void computeConstant() {
		// this.value is pre-computed, just use that:
		this.constant = IntConstant.fromValue(this.value);
	}
	
	@Override
	public TypeBinding resolveType(BlockScope scope) {
		char[] trimmed = CharOperation.trim(source());
		for (int l=0; l<numbers.length; l+=2) {
			char[][] langNumbers = numbers[l+1];
			for (int i=0; i<langNumbers.length; i++) {
				if (CharOperation.equals(langNumbers[i], trimmed)) {
					this.value = i;
					this.language = new String(numbers[l][0]);
					return super.resolveType(scope);
				}
			}
		}
		this.constant = Constant.NotAConstant;
		scope.problemReporter().parseErrorReplaceToken(this.sourceStart, this.sourceEnd, TerminalTokens.TokenNamenull, trimmed, "Unrecognized", "CustomIntLiteral");
		return TypeBinding.INT;
	}
}
